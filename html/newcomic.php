<?php
require_once("./phplib/common.php");
require_once("./phplib/auth.php");
require_once("./phplib/db.php");

require_admin();
commonStart('New Comic',["admin"]);

$series = db->get_all_series();

$isedit = array_key_exists('id',$_GET);
$comic = null;
if($isedit){
	/** @var Comic $comic */
	$comic = db->get_comic($_GET['id']);
}
?>
<h2>So you wanna make new comic?</h2>
<form action="./api.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="c" value="<?=$isedit ? 'editcomic' : 'newcomic' ?>"/>
	<?php if($isedit){ ?>
		<input type="hidden" name="id" value="<?=$comic->id?>"/>
	<?php } ?>
	<label>Name:</label> <input name="name" required value="<?=$isedit ? htmlspecialchars($comic->name) : ''?>"/><br>
	<label>Series:</label> <select name="series" required>
		<option value="" disabled hidden<?=$isedit ? '' : ' selected'?>></option>
		<?php
			foreach($series as $s){
				$selectstr = $isedit && $s['id']==$comic->seriesid ? ' selected' : '';
				$name = htmlspecialchars($s['name']);
				echo "<option value='{$s['id']}'{$selectstr}>{$name}</option>";
			}
		?>
	</select><br>
	<label>Description:</label> <textarea name="description"><?=
		$isedit ? htmlspecialchars($comic->description) : ''
	?></textarea><br>
	<label>Alt Text:</label> <textarea name="alt"><?=
		$isedit ? htmlspecialchars($comic->alt) : ''
	?></textarea><br>
	<label>File:</label> <input type="file" name="file" accept="image/png"<?=$isedit ? '' : ' required'?>/><br>
	<br>
	<input type="submit" value="Submit Comic"/>
</form>