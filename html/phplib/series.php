<?php

class Series {
	public ?int $id;//null id is pseudo-series "all"
	public string $name;
	public ?int $firstcomicid;
	public ?int $lastcomicid;
	/**
	 * constructs series with data from the
	 * @param array $dbswd db series with data
	 */
	public function __construct(array $dbswd){
		$this->id = $dbswd['id'];
		$this->name = $dbswd['name'];
		$this->firstcomicid = $dbswd['firstcomicid'];
		$this->lastcomicid = $dbswd['lastcomicid'];
	}
}