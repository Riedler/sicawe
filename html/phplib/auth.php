<?php
require_once('CONFIG.php');

session_start();
if(array_key_exists('passwd',$_POST)){
	$_SESSION['privileged'] = hash('sha256',$_POST['passwd']) == adminpasswdhash;
}else if(!array_key_exists('privileged',$_SESSION)){
	$_SESSION['privileged'] = false;
}

function require_admin(){
	if(!$_SESSION['privileged']){
		require_once('common.php');
		
		commonStart('Login',[]);
		?>

<h2>Login ❤️</h2>
<form method="POST" action="">
	<input type="password" name="passwd"/>
</form>

		<?php
		if(array_key_exists('passwd',$_POST)){
			echo "<span>Your hash: <pre>".hash('sha256',$_POST['passwd'])."</pre></span>";
		}
		commonEnd();
		
		http_response_code(403);
		die();
	}
}