<?php

class Comic {
	public int $id;
	public int $seriesid;
	public string $seriesname;
	public string $name;
	public string $description;
	public string $alt;
	public string $uploaddate; //do I look like I want to handle dates? no!
	public ?int $nextcomicinseriesid;
	public ?int $nextcomicoverallid;
	public ?int $prevcomicinseriesid;
	public ?int $prevcomicoverallid;
	/**
	 * constructs comic with data from the
	 * @param array $dbcwd db comic with data
	 */
	public function __construct(array $dbcwd){
		$this->id=$dbcwd['id'];
		$this->seriesid=$dbcwd['seriesid'];
		$this->seriesname=$dbcwd['seriesname'];
		$this->name=$dbcwd['name'];
		$this->description=$dbcwd['description'];
		$this->alt=$dbcwd['alt'] ?? '';
		$this->uploaddate=$dbcwd['uploaddate'];
		$this->nextcomicinseriesid=$dbcwd['nextcomicinseriesid'];
		$this->nextcomicoverallid=$dbcwd['nextcomicoverallid'];
		$this->prevcomicinseriesid=$dbcwd['prevcomicinseriesid'];
		$this->prevcomicoverallid=$dbcwd['prevcomicoverallid'];
	}
}