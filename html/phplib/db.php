<?php

require_once("CONFIG.php");
require_once("comic.php");
require_once("series.php");

//NOTE: partially taken from riedler.wien

class _db {
	public $db = null;
	public function __construct(){
		if($this->db!==null) return;
		
		$this->db = new PDO('mysql:host='.dbhostname.';dbname='.dbname.';charset=utf8mb4',dbuser,dbpasswd);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	/**
	 * get PDO query
	 * NOTE: all the parameters are strings in this case
	 * @param string $query the query to be executed
	 * @param array $args the parameters for the query, to be inserted in place of the question marks
	 * @return PDOStatement|false the created PDO statement, to fetch data from
	 */
	public function get_pq(string $query,array $args): PDOStatement|false{
		$stmt = $this->db->prepare($query);
		$stmt->execute($args);
		return $stmt;
	}
	/**
	 * get typed PDO query
	 * @param string $query the query to be executed
	 * @param array $args the parameters for the query, to be inserted in place of the question marks
	 * @param array $types the types of the query parameters
	 * @return PDOStatement|false the created PDO statement, to fetch data from
	 */
	public function get_tpq(string $query,array $args,array $types): PDOStatement|false{
		$stmt = $this->db->prepare($query);
		for($i=0;$i<count($args);$i++){
			$stmt->bindparam($i+1,$args[$i],$types[$i]);
		}
		$stmt->execute();
		return $stmt;
	}
	/**
	 * creates a new series
	 */
	public function add_series(string $name){
		$this->get_pq("INSERT INTO series (name) VALUES (?)",[$name])->fetch();
	}
	/**
	 * deletes a series
	 */
	public function del_series(int $id){
		$this->get_tpq("DELETE FROM series WHERE id=?",[$id],[PDO::PARAM_INT])->fetch();
	}
	/**
	 * returns all series with id and name
	 */
	public function get_all_series(): array{
		return $this->get_pq("SELECT id,name FROM series",[])->fetchAll();
	}
	/**
	 * returns all series with data
	 */
	public function get_all_series_data(): array{
		return $this->get_pq("SELECT * FROM series_with_data",[])->fetchAll();
	}
	/**
	 * returns series as object
	 */
	public function get_series(int $id): Series|null {
		$data = $this->get_tpq("SELECT * FROM series_with_data WHERE id=?",[$id],[PDO::PARAM_INT])->fetch();
		if($data==false){
			return null;
		}else{
			return new Series($data);
		}
	}
	/**
	 * Returns comic as object
	 * @param int $id the comic's ID
	 */
	public function get_comic(int $id): Comic|null {
		$data = $this->get_tpq("SELECT * FROM comics_with_data WHERE id=?",[$id],[PDO::PARAM_INT])->fetch();
		if($data==false){
			return null;
		}else{
			return new Comic($data);
		}
	}
	/**
	 * Adds a comic entry
	 * @param int $seriesid the ID of the series to add this to
	 * @param string $name the title/name of this comic entry
	 * @param string $description a description for this comic entry
	 * @param string $alt the alt text
	 * @return int the ID of the new comic
	 */
	public function add_comic(int $seriesid, string $name, string $description, string $alt) : int {
		$this->get_tpq(
			"INSERT INTO comics (seriesid,name,description,alt,uploaddate) VALUES (?,?,?,?,NOW())",
			[$seriesid,$name,$description,$alt],
			[PDO::PARAM_INT,PDO::PARAM_STR,PDO::PARAM_STR,PDO::PARAM_STR]
		)->fetch();
		return $this->db->lastInsertId();
	}
	/**
	 * Sets a comic entry
	 * @param int $seriesid the ID of the series to add this to
	 * @param string $name the title/name of this comic entry
	 * @param string $description a description for this comic entry
	 * @param string $alt the alt text
	 * @param string $date the timedate string
	 * @return int the ID of the new comic
	 */
	public function set_comic(int $id,int $seriesid, string $name, string $description, string $alt, string $date) : int {
		$this->get_tpq(
			"INSERT INTO comics (id,seriesid,name,description,alt,uploaddate) VALUES (?,?,?,?,?,?)",
			[$id, $seriesid, $name, $description, $alt, $date],
			[PDO::PARAM_INT, PDO::PARAM_INT, PDO::PARAM_STR, PDO::PARAM_STR, PDO::PARAM_STR, PDO::PARAM_STR]
		)->fetch();
		return $this->db->lastInsertId();
	}
	/**
	 * Deletes comic entry
	 * @param int $id the comic's ID
	 */
	public function del_comic(int $id) {
		$this->get_tpq("DELETE FROM comics WHERE id=?",[$id],[PDO::PARAM_INT])->fetch();
	}
	/**
	 * Returns first comic's id
	 */
	public function get_first_comic_id(): int {
		return $this->get_pq("SELECT id FROM comics ORDER BY id ASC LIMIT 1",[])->fetch()['id'];
	}
	/**
	 * Returns last comic's id
	 */
	public function get_last_comic_id(): int {
		return $this->get_pq("SELECT id FROM comics ORDER BY id DESC LIMIT 1",[])->fetch()['id'];
	}
	/**
	 * Returns id, name and uploaddate of all comics in a series
	 * WARNING: potentially database intensive, use sparingly
	 */
	public function get_all_comics_in_series(int $seriesid): array {
		return $this->get_pq("SELECT id,name,uploaddate FROM comics WHERE seriesid=? ORDER BY id ASC",[$seriesid],[PDO::PARAM_INT])->fetchAll();
	}
}

const db = new _db();
