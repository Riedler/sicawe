<?php

const dbhostname = 'localhost';
const dbname = 'maindb';
const dbuser = 'webuser';
const dbpasswd = 'passwd';

const adminpasswdhash = '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918';

// prefix for all links, without trailing slash
const urlroot = '';

// some strings for internal use
const sitename = 'sicawe'; // used as prefix for all page titles
const tagline = 'A Webcomic!'; // used inside the page title of the actual reader