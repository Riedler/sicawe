<?php

require_once("db.php");

/**
 * Prints all the stuff that comes before the main content
 * @param string $title the title the page should have
 * @param array $addCSS array with css stylesheets to be loaded
 */
function commonStart(string $title, array $addCSS) {

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title><?=htmlspecialchars(sitename.': '.$title)?></title>
		<base href="<?=urlroot?>/"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image/png" href="./resources/favicon.png"/>
		<?php
			array_push($addCSS,'default');
			array_push($addCSS,'custom');
			foreach($addCSS as $cssname){
				?><link rel='stylesheet' href='./css/<?=$cssname?>.css'><?php
			}
		?>
		<script async src="./js/default.js"></script>
	</head>
	<body>
<?php

}

/**
 * Prints all the stuff that comes after the main content
 */
function commonEnd() {

?>
	</body>
</html>
<?php

}

/**
 * prints the header plus navbar
 * @param int|null $current_series the currently selected comic series, or null if none are selected
 * @param string $current_page the currently loaded page's name, or '' if unnamed
 */
function commonHeader(int|null $current_series=null, string $current_page='') {
?>
<div id="header">
	<a href="./" aria-label="Home">
		<img src="./resources/header.png" aria-hidden="true"/>
	</a>
	<nav>
		<a<?=$current_page=='about' ? ' class="current"' : ''?> href="./about/">About</a>
		<?php
			$series = db->get_all_series_data();
			foreach($series as $s){
				if($s['comiccount']>0) {
					$name = htmlspecialchars($s['name']);
					$current = $current_series == $s['id'] ? ' class="current"' : '';
					echo "<a{$current} href=\"./?series={$s['id']}\">{$name}</a>";
				}
			}
		?>
	</nav>
</div>
<?php
}

function commonComicArrows(Comic $c,Series $s){
	$first = $s->firstcomicid;
	$prev = $s->id===null ? $c->prevcomicoverallid : $c->prevcomicinseriesid;
	$next = $s->id===null ? $c->nextcomicoverallid : $c->nextcomicinseriesid;
	$last = $s->lastcomicid;
	if($last==$c->id)
		$last=null;
	if($first==$c->id)
		$first=null;
?>
<nav>
	<a	class="firstbtn revicon<?= $first ? '' : ' disabled' ?>"
		aria-label="First Page<?=$first ? '' : ' (disabled)'?>"
		href="./?series=<?=$s->id?>&amp;comic=<?=$first?>"
		><?=file_get_contents(dirname(__DIR__).'/resources/last.svg')
	?></a>
	<a	class="prevbtn revicon<?= $prev ? '' : ' disabled' ?>"
		aria-label="Previous Page<?=$prev ? '' : ' (disabled)'?>"
		href="./?series=<?=$s->id?>&amp;comic=<?=$prev?>"
		><?=file_get_contents(dirname(__DIR__).'/resources/next.svg')
	?></a>
	<a	class="nextbtn<?= $next ? '' : ' disabled' ?>"
		aria-label="Next Page<?=$next ? '' : ' (disabled)'?>"
		href="./?series=<?=$s->id?>&amp;comic=<?=$next?>"
		><?=file_get_contents(dirname(__DIR__).'/resources/next.svg')
	?></a>
	<a	class="lastbtn<?= $last ? '' : ' disabled' ?>"
		aria-label="Last Page<?=$last ? '' : ' (disabled)'?>"
		href="./?series=<?=$s->id?>&amp;comic=<?=$last?>"
		><?=file_get_contents(dirname(__DIR__).'/resources/last.svg')
	?></a>
</nav>
<?php
}

/**
 * prints the comic container
 */
function commonComicContainer(Comic $c,Series $s){
?>
<div class="comic card">
	<?php commonComicArrows($c,$s) ?>
	<img class="comic__inner" alt="<?=htmlspecialchars($c->alt)?>" src="./images/<?=$c->id?>.png"/>
	<?php commonComicArrows($c,$s) ?>
</div>
<?php
}

function commonComicInfobox(Comic $c){
?>
<div class="infobox card">
	<h1><?=htmlspecialchars($c->name)?></h1>
	<p>
		<?=nl2br(htmlspecialchars($c->description))?>
	</p>
</div>
<?php
}
