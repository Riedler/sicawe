window.addEventListener('load',()=>{
	let comics = document.getElementsByClassName('comic');
	if(comics.length==1){
		let comic = comics[0];
		
		function pressarrow(name){
			let btn = comic.getElementsByClassName(name+'btn')[0];
			if(!btn.classList.contains('disabled'))
				btn.click();
		}
		
		document.addEventListener('keyup',(event)=>{
			if(event.key=='ArrowRight'){
				if(event.ctrlKey){
					pressarrow('last');
				}else{
					pressarrow('next');
				}
			}else if(event.key=='ArrowLeft'){
				if(event.ctrlKey){
					pressarrow('first');
				}else{
					pressarrow('prev');
				}
			}
		});
		console.log("ew");
	}
});