<?php
require_once('./phplib/auth.php');
require_once('./phplib/db.php');

require_admin();

function validate_file($f) {
	//TODO: check type of $f
	//TODO: check mimetype rather than file extension
	$ext = strtolower(pathinfo($f['name'],PATHINFO_EXTENSION));
	if($ext!=='png'){
		echo "only png is accepted! you uploaded: ".$ext;
		http_response_code(415);
		die();
	}
}

$params = null;

if(array_key_exists('c',$_GET)){
	$params = $_GET;
}else if(array_key_exists('c',$_POST)){
	$params = $_POST;
}else{
	echo "No command found!";
	http_response_code(400);
	die();
}

$command = $params['c'];

switch($command){
	case 'newseries':
		db->add_series($params['name']);
		break;
	case 'delseries':
		db->del_series($params['id']);
		break;
	case 'newcomic':
		$f = $_FILES['file'];
		validate_file($f);
		
		$id = db->add_comic($params['series'],$params['name'],$params['description'],$params['alt']);
		
		move_uploaded_file($f["tmp_name"], "./images/{$id}.png");
		break;
	case 'editcomic':
		$comic = db->get_comic($params['id']);
		if($comic == null){
			echo "no comic with ID ".$params['id'];
			http_response_code(404);
		}
		//replace image if uploaded
		if(array_key_exists('file',$_FILES) && $_FILES['file']['size']>0){
			$f = $_FILES['file'];
			validate_file($f);
			unlink("./images/{$params['id']}.png");
			move_uploaded_file($f["tmp_name"], "./images/{$params['id']}.png");
		}
		//replace all other data, since it's autofilled anyway
		db->del_comic($params['id']);
		db->set_comic($params['id'],$params['series'],$params['name'],$params['description'],$params['alt'],$comic->uploaddate);
		break;
	case 'delcomic':
		db->del_comic($params['id']);
		unlink("./images/{$params['id']}.png");
		break;
	default:
		echo "unknown command: ".$command;
		die();
}

// redirect back to admin page if no error was thrown
header('Location: '.urlroot.'/admin.php');
die();