<?php
require_once("../phplib/common.php");

commonStart('About',['about']);
commonHeader(current_page:'about');

?>

<main>
	<p class="about card">
		Made by Riedler <img style="display:inline-block;vertical-align:top" width="16" src="https://riedler.wien/favicon.svg" aria-hidden="true"/><br/>
		Uhh placeholder placeholder more about <img style="display:inline-block;vertical-align:bottom" height="16" src="./resources/header.png" alt="sicawe"/> here!!
	</p>
</main>

<?php

commonEnd();
