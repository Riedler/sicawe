<?php
require_once("phplib/common.php");
require_once("phplib/db.php");

$seriesid = array_key_exists('series',$_GET) ? $_GET['series'] : null;
$series = null;
if ($seriesid != null && is_numeric($seriesid)) {
	$series = db->get_series((int)$seriesid);
}
if ($series == null) {
	$series = new Series([
		'id' => null,
		'name' => 'ALL',
		'firstcomicid' => db->get_first_comic_id(),
		'lastcomicid' => db->get_last_comic_id(),
	]);
	$seriesid = $series->id;
}

$comicid = array_key_exists('comic',$_GET) ? $_GET['comic'] : null;
$comic = null;
if($comicid!=null && is_numeric($comicid)){
	$comic = db->get_comic((int)$comicid);
}
if($comic == null){
	$comic = db->get_comic($series->lastcomicid);
}

commonStart(tagline,['main']);

commonHeader($seriesid,'main');

?>

<main>
	<?php
		commonComicContainer($comic,$series);
		commonComicInfobox($comic);
	?>
</main>

<?php

commonEnd();
