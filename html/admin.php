<?php
require_once("./phplib/common.php");
require_once("./phplib/auth.php");
require_once("./phplib/db.php");

require_admin();
commonStart('Admin',["admin"]);

?>
<fieldset>
	<legend>New Series</legend>
	<form action="./api.php">
		<input type="hidden" name="c" value="newseries"/>
		<input type="text" name="name" placeholder="blah" required/> <label>Name</label><br/>
		<br/>
		<button>Submit</button>
	</form>
</fieldset>
<span>→ <a href="./newcomic.php">New Comic</a></span><br/>
<?php

$series = db->get_all_series();
foreach($series as $s) {
	?>
	<h2 class="series__name"><?=htmlspecialchars($s['name'])?></h2>
	<a class="delseries" href="./api.php?c=delseries&amp;id=<?=$s['id']?>">delete</a>
	<div class="comicgrid">
		<b>ID</b><b>Name</b><b>Upload Date</b>
		<?php
			foreach(db->get_all_comics_in_series($s['id']) as $comic){
				$name = htmlspecialchars($comic['name']);
				echo "<span class='comic__id'>{$comic['id']}</span>";
				echo "<span class='comic__name'>{$name}</span>";
				echo "<span class='comic__date'>{$comic['uploaddate']}</span>";
				echo "<a class='comic__edit' href='./newcomic.php?id={$comic['id']}'>edit</a>";
				echo "<a class='comic__delete' href='./api.php?c=delcomic&amp;id={$comic['id']}'>delete</a>";
			}
		?>
	</div>
<?php
}

commonEnd();