## What? Huh???

This is a generic minimalist comic reader website. Mainly for [Heatherhorns](https://plush.city/@heatherhorns)'s feywads comic, but it can be adapted for other things.

### ahh, so it's like that, huh. I understand everything now.

ok, good! If you want to use it and need to add features, I'd love to see if they're upstream-able, so PRs are always welcome ^^

## Setup

As a first step, have a look at the Vagrantfile for a real setup, or simply pull it up virtually.

After that, you still have to do a few manual steps.

### Error reporting

For dev setups, you'll want errors to get printed. So, find the `display_errors` setting in php.ini and set it to `on`.

### File uploading

To be able to upload files, tell php to allow it by setting `file_uploads` to `On` in php.ini.
Also note that you might need to give the php webuser access to the html/images/ directory for uploads to work.

### Database

Simply execute the db.sql file in the empty database you set up earlier.

### CONFIG.php

Have a look at all the constants in html/phplib/CONFIG.php and set them to values that correspond to what your setup needs.

*HINT:* the adminpassword directive is best set by entering the password in the admin panel while the website is already running, and copying the hashsum that gets printed in the error message.
For development: the default password is 'admin'. I *highly* recommend changing it for prod though.

### Branding

The default branding is very, uh, bad. This is on purpose; you're supposed to replace images in html/resources/ with your own.

There's also a css file in html/css/custom.css where you can add your own colors, custom fonts, and other adjustments easily - it's automatically included on every page.

## Admin

You can access the (very crude atm) admin panel via /admin.php - everything there should be self-explanatory.

### Migration / Updating the server

migrating between versions is relatively easy & quick ^^

First, you'll need to find out if there's been any database changes. Look into migration.sql and see execute any snippets that are newer than your current version. Keep your eye out for comments, sometimes extra steps are needed!

Second, you copy all the files over. Here, we need to keep in mind which folders aren't stock, i.e. modified by the server or your custom setup. By default, the only files and folders you can't blindly update are:

- `html/images/` - this is where your comics are stored.
- `html/resources/` - contains your custom branding images.
- `html/phplib/CONFIG.php` - this is your server-specific config.
- `html/css/custom.css` - this is your custom branding stylesheet.

Do check for changes there, and act accordingly. Everything else you can just blindly copy paste replace, assuming you didn't make your own custom changes anywhere else.