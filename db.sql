CREATE TABLE series (
	id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
	name TINYTEXT UNIQUE NOT NULL
);

CREATE TABLE comics (
	id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
	seriesid int,
	name TINYTEXT UNIQUE NOT NULL,
	description TEXT,
	alt TEXT,
	uploaddate date,
	FOREIGN KEY (seriesid) REFERENCES series(id)
);

CREATE VIEW comics_with_data AS
SELECT
	id,
	seriesid,
	(SELECT name FROM series WHERE series.id = comics.seriesid) AS seriesname,
	name,
	description,
	uploaddate,
	alt,
	(SELECT id FROM comics ncomics WHERE ncomics.id>comics.id AND ncomics.seriesid=comics.seriesid ORDER BY ncomics.id ASC LIMIT 1) as nextcomicinseriesid,
	(SELECT id FROM comics ncomics WHERE ncomics.id>comics.id ORDER BY ncomics.id ASC LIMIT 1) as nextcomicoverallid,
	(SELECT id FROM comics pcomics WHERE pcomics.id<comics.id AND pcomics.seriesid=comics.seriesid ORDER BY pcomics.id DESC LIMIT 1) as prevcomicinseriesid,
	(SELECT id FROM comics pcomics WHERE pcomics.id<comics.id ORDER BY pcomics.id DESC LIMIT 1) as prevcomicoverallid
FROM comics;

CREATE VIEW series_with_data AS
SELECT
	id,
	name,
	(SELECT id FROM comics WHERE comics.seriesid=series.id ORDER BY comics.id ASC LIMIT 1) as firstcomicid,
	(SELECT id FROM comics WHERE comics.seriesid=series.id ORDER BY comics.id DESC LIMIT 1) as lastcomicid,
	(SELECT COUNT(id) FROM comics WHERE comics.seriesid=series.id) as comiccount
FROM series;