/**
 * NOTE: IMPORTANT!!
 * 
 * this file isn't meant for direct execution, but as a collection of snippets
 * that help migrating the database to newer versions
 */

/* 2023.10.29 */
ALTER TABLE comics ADD COLUMN alt TEXT;

DROP VIEW comics_with_data;
CREATE VIEW comics_with_data AS
SELECT
	id,
	seriesid,
	(SELECT name FROM series WHERE series.id = comics.seriesid) AS seriesname,
	name,
	description,
	uploaddate,
	alt,
	(SELECT id FROM comics ncomics WHERE ncomics.id>comics.id AND ncomics.seriesid=comics.seriesid ORDER BY ncomics.id ASC LIMIT 1) as nextcomicinseriesid,
	(SELECT id FROM comics ncomics WHERE ncomics.id>comics.id ORDER BY ncomics.id ASC LIMIT 1) as nextcomicoverallid,
	(SELECT id FROM comics pcomics WHERE pcomics.id<comics.id AND pcomics.seriesid=comics.seriesid ORDER BY pcomics.id DESC LIMIT 1) as prevcomicinseriesid,
	(SELECT id FROM comics pcomics WHERE pcomics.id<comics.id ORDER BY pcomics.id DESC LIMIT 1) as prevcomicoverallid
FROM comics;
